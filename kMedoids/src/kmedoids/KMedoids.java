/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kmedoids;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author serdarozay
 */
public class KMedoids {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        if (args == null || args.length < 1) {
            System.out.println("Parametre girişinde eksik var. Lütfen doğru parametrelerle  tekrar çalıştırınız");
            System.exit(0);
        }

        int clusterCount = Integer.parseInt(args[0]);


        double[][] decisionMatx = ReadDecisionMatrix();

        if (decisionMatx == null || decisionMatx.length != decisionMatx[0].length) {
            System.out.println("Decision matrix verileri düzgün değil. Program sonlanacaktır.");
            System.exit(0);
        }

        ExecuteKmedoids(clusterCount, decisionMatx);



    }

    private static double[][] ReadDecisionMatrix() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static void ExecuteKmedoids(int clusterCount, double[][] decisionMatx) {

        int[] centeroidsAndRandomCenter = GetRandomCenters(decisionMatx.length);
        int[] centroids = GetCentroids(centeroidsAndRandomCenter);
        int randomCenter = centeroidsAndRandomCenter[centeroidsAndRandomCenter.length - 1];


        double cost = Double.MAX_VALUE;
        Boolean ChangeState = true;
        HashMap<Integer, List<Integer>> clusters = null;

        while (ChangeState) {
            ChangeState = false;
            for (int i = 0; i < centroids.length + 1; i++) {

                if (clusters != null && i == 0) {
                    randomCenter = renewRandomCenter(randomCenter,centroids);
                    continue;
                }
                
                int[] centroidsClone = centroids.clone();

                if (i > 0) {
                    centroidsClone[i - 1] = randomCenter;
                }

                HashMap<Integer, List<Integer>>  clustersTemp = new HashMap<Integer, List<Integer>>();
                clustersTemp = GetClusters(centroidsClone, decisionMatx);
                double costTemp = calculateCost(centroidsClone, clustersTemp, decisionMatx);
                
                if (costTemp < cost) {
                    cost = costTemp;
                    clusters = clustersTemp;
                    
                    if (i != 0) {
                        ChangeState = true;
                        centroids = centroidsClone;
                        
                        break;
                    }
                }
            }
        }
        writeScreenTo(centroids,clusters);

    }

    private static int[] GetRandomCenters(int length) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static int[] GetCentroids(int[] centeroidsAndRandomCenter) {
        // sonuncuyu çıakrıp ger gönder
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static HashMap<Integer, List<Integer>> GetClusters(int[] centroids, double[][] decisionMatx) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static double calculateCost(int[] centroids, HashMap<Integer, List<Integer>> clusters, double[][] decisionMatx) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static void writeScreenTo(int[] centroids, HashMap<Integer, List<Integer>> clusters) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static int renewRandomCenter(int randomCenter, int[] centroids) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
