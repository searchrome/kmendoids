
import java.io.*;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

/**
 *
 * @author serdarozay
 */
public class IDTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*
         * if (args.length < 1) { System.out.println("Lütfen program
         * parametrelerini giriniz."); return; }
         *
         * if (args[0].equals("buildTree")) { buildDecisionTree(); }
         *
         * if (args[0].equals("makeDecision")) { if (args.length != 3) {
         * System.out.println("Lütfen program doğru giriniz.Parametrelerini
         * giriniz."); return; }
         *
         * String xmlFile = args[1]; String values = args[2];
         *
         * makeDecision(xmlFile, values);
        }
         */
        //buildDecisionTree();
        makeDecision("file.xml","sunny cool normal weak");
    }

    private static void makeDecision(String xmlFile, String values) {
        String[] columns = values.split(" ");

        try {

            File fXmlFile = new File(xmlFile);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            Element el = doc.getDocumentElement();
            getDecision(el, columns);
           

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void getDecision(Element el,String[] columns)
    {
           
            int column = Integer.parseInt(el.getAttribute("columnIndex"));
            String filter = columns[column];

            NodeList nList = el.getChildNodes();
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) nNode;
                    String cond = e.getAttribute("condition");
                    if (cond.equals(filter)) {
                        String dec =  e.getAttribute("decision");
                        if (dec != "") {
                            System.out.println("Decision:" + dec);
                        } else {
                            getDecision(e,columns);
                        }
                        break;
                    }
                }
            }
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }

    private static void buildDecisionTree() {
        try {
            List<String[]> trans = GetTransactionFromFile();

            String defTrue = "";
            String defFalse = "";
            for (String[] val : trans) {
                if (defTrue.equals("")) {
                    defTrue = val[val.length - 1];
                }

                if (!defTrue.equals("") && !val[val.length - 1].equals(defTrue)) {
                    defFalse = val[val.length - 1];
                    break;
                }
            }


            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();

            BuildTree(trans, 0, -1, "Root", defTrue, defFalse, doc, null);


        } catch (Exception e) {
            System.out.println("hata:" + e.toString());
        }
    }

    private static void BuildTree(List<String[]> trans, int level, int removeColumn, String filterColumn,
            String defaultTrue, String defaultFalse, Document doc, Element parent) throws TransformerConfigurationException, TransformerException {

        if (level > 0) {
            trans = removeTransColumn(trans, removeColumn, filterColumn);
            if (trans == null || trans.size() == 0) {
                System.out.println("node finish");
                return;
            }
        }

        HashMap<Integer, HashMap<String, AttTypeStatistic>> statistics = getStatictic(trans, defaultTrue);

        int index = getMaxEntropyAttributeIndex(statistics);
        System.out.println("Found Tree Level(" + level + ") Root Column index" + index);

        parent = createNode(doc, (level + index), parent, filterColumn);



        HashMap<String, AttTypeStatistic> workingRoot = statistics.get(index);
        for (String key : workingRoot.keySet()) {
            System.out.println("Found Sub Level:" + key);
            AttTypeStatistic attr = workingRoot.get(key);
            if (attr.getClassFalse() == 0) {
                System.out.println("Sub Level:" + key + " Ends with :" + defaultTrue);
                addAttributeNode(doc, parent, key, defaultTrue);
            } else if (attr.getClassTrue() == 0) {
                System.out.println("Sub Level:" + key + " Ends with :" + defaultFalse);
                addAttributeNode(doc, parent, key, defaultFalse);
            } else {
                System.out.println("T:" + attr.getClassTrue() + " F:" + attr.getClassFalse());
                BuildTree(trans, level + 1, index, key, defaultTrue, defaultFalse, doc, parent);
            }

        }
        if (level == 0) {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("file.xml"));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");
        }

    }

    private static Element createNode(Document doc, int index, Element parent, String columnName) throws DOMException {
        Element rootElement = doc.createElement("node");

        Attr at = doc.createAttribute("columnIndex");
        at.setValue("" + index);
        rootElement.setAttributeNode(at);


        if (parent == null) {
            doc.appendChild(rootElement);
        } else {
            at = doc.createAttribute("condition");
            at.setValue(columnName);
            rootElement.setAttributeNode(at);
            parent.appendChild(rootElement);
        }
        return rootElement;
    }

    private static void addAttributeNode(Document doc, Element parent, String value, String className) throws DOMException {
        Element rootElement = doc.createElement("decisionNode");

        Attr at = doc.createAttribute("condition");
        at.setValue(value);
        rootElement.setAttributeNode(at);
        if (className != null) {
            at = doc.createAttribute("decision");
            at.setValue(className);
            rootElement.setAttributeNode(at);
        }

        parent.appendChild(rootElement);
    }

    private static int getMaxEntropyAttributeIndex(HashMap<Integer, HashMap<String, AttTypeStatistic>> statistics) {
        int maxEntropyIndex = 0;
        double maxEntropy = -1000.0;
        for (Integer key : statistics.keySet()) {

            HashMap<String, AttTypeStatistic> types = statistics.get(key);
            double val = getEntropy(types);
            if (val == 0) {
                System.out.println("leaf");
            }
            if (val > maxEntropy) {
                maxEntropyIndex = key;
                maxEntropy = val;
            } else if (val == maxEntropy) {
                System.out.println("ayni entropy ile kayit bulundu, ilk bulunan dogru kabul edildi.");
                //TODO: bilgi aktarilmali .. hangisi ? 
            }
        }
        return maxEntropyIndex;
    }

    private static double getEntropy(HashMap<String, AttTypeStatistic> types) {
        double entropy = 0.0;
        double entropyWithClass = 0.0;
        for (AttTypeStatistic type : types.values()) {
            double posibility = type.getCount() * 1.0 / type.getSampleSize();
            double truePosibility = type.getClassTrue() * 1.0 / type.getSampleSize();
            double falsePosibility = type.getClassFalse() * 1.0 / type.getSampleSize();
            if (posibility != 0) {
                entropy = entropy - (posibility * logOfBase(2, posibility));
            }

            entropyWithClass = entropyWithClass - (truePosibility == 0 ? 0 : (truePosibility * logOfBase(2, truePosibility)))
                    - (falsePosibility == 0 ? 0 : (falsePosibility * logOfBase(2, falsePosibility)));
        }

        return entropy - entropyWithClass;
    }

    private static double logOfBase(int base, double num) {
        return Math.log(num) / Math.log(base);
    }

    public static HashMap<Integer, HashMap<String, AttTypeStatistic>> getStatictic(List<String[]> trans, String defaultTree) {
        if (trans == null || trans.size() == 0) {
            return null;
        }

        int attributeCount = trans.get(0).length - 1;
        int sampleCount = trans.size();

        HashMap<Integer, HashMap<String, AttTypeStatistic>> statistic = new HashMap<Integer, HashMap<String, AttTypeStatistic>>();

        for (String[] attributes : trans) {
            if (attributes == null) {
                continue;
            }
            String className = attributes[attributeCount];

            for (int i = 0; i < attributeCount; i++) {
                if (statistic.get(i) == null) {
                    //AttTypeStatistic st = new AttTypeStatistic(attributes[i], defaultTrue,sampleCount);
                    statistic.put(i, new HashMap<String, AttTypeStatistic>());
                }
                HashMap<String, AttTypeStatistic> types = statistic.get(i);
                if (types.containsKey(attributes[i])) {
                    types.get(attributes[i]).countValue(className);
                } else {
                    AttTypeStatistic st;
                    st = new AttTypeStatistic(attributes[i], defaultTree, sampleCount);
                    types.put(attributes[i], st);
                    types.get(attributes[i]).countValue(className);
                }
            }
        }


        return statistic;

    }

    private static List<String[]> GetTransactionFromFile() {
        try {

            FileInputStream fstream = new FileInputStream("samples.txt");

            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;

            List<String[]> trans = new ArrayList<String[]>();
            while ((strLine = br.readLine()) != null) {

                SortedSet<String> items = new TreeSet<String>();
                String[] values = strLine.split(" ");
                trans.add(values);
            }

            in.close();
            return trans;
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            return null;
        }
    }

    private static List<String[]> removeTransColumn(List<String[]> trans, int removeColumn, String filter) {
        List<String[]> newList = new ArrayList<String[]>();
        for (String[] val : trans) {
            if (val[removeColumn].equals(filter)) {
                String[] nVal = new String[val.length - 1];
                int rInd = -1;
                for (int i = 0; i < val.length; i++) {
                    if (i != removeColumn) {
                        rInd++;
                        nVal[rInd] = val[i];
                    }
                }
                newList.add(nVal);
            }
        }
        return newList;
    }
}

class AttTypeStatistic {

    Integer[] counts = new Integer[]{0, 0, 0};
    String defTrue;
    String attName;
    int samplesize;

    public AttTypeStatistic(String name, String defaultTrue, int listSize) {
        attName = name;
        defTrue = defaultTrue;
        samplesize = listSize;
    }

    public void countValue(String className) {
        counts[0]++;
        if (className.equals(defTrue)) {
            counts[1]++;
        } else {
            counts[2]++;
        }
    }

    public int getCount() {
        return counts[0];
    }

    public int getClassTrue() {
        return counts[1];
    }

    public int getClassFalse() {
        return counts[2];
    }

    public int getSampleSize() {
        return samplesize;
    }
}
